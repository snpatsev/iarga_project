(function ($) {

    jQuery.expr.filters.offscreen = function (el) {
        var rect = el.getBoundingClientRect();
        return (
            (rect.x + rect.width) < 0 ||
            (rect.y + rect.height) < 0 ||
            (rect.x > window.innerWidth || rect.y > window.innerHeight)
        );
    };

    $(function () {

        // Если нет поддержки тега <svg>
        if (!Modernizr.inlinesvg) {

            $('[data-png]').each(function () {

                var src = $(this).data('png');

                $(this).before($('<img src="' + src + '" alt="">')).remove();

            });

        }


        // Если нет поддержки картинки image.svg

        if (!Modernizr.svgasimg) {

            $('img[src $= ".svg"]').each(function () {

                var src = $(this).attr('src');

                $(this).attr('src', src.replace('.svg', '.png'));

            });

        }
    });

    // Слайдеры

    $.carouselReviewCard = function () {

        var $el = $('.js-carousel-review-card');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            //            autoPlay: 5500,
            prevNextButtons: true,
            pageDots: false,
            wrapAround: true,
            lazyLoad: 1,
            imagesLoaded: true,
            selectedAttraction: 0.01,
            friction: 0.15,
            arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
        });
    }

    $.carouselGallery = function () {

        var $el = $('.js-carousel-gallery'),
            $elAmount;

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                cellSelector: '.carousel-item',
                //            autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                lazyLoad: 6,
                imagesLoaded: true,
                selectedAttraction: 0.01,
                friction: 0.15,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $elAmount = $(this).find('.flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next());

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                groupCells: 5,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: true,
                contain: true,
                prevNextButtons: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                pageDots: false
            });

            $(this).find('.flickity-button').appendTo($(this).next());

        });
    }

    $.carouselProduct = function () {

        var $el = $('.js-carousel-product');

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                cellSelector: '.carousel-item',
                //            autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                lazyLoad: 2,
                adaptiveHeight: true,
                imagesLoaded: true,
                selectedAttraction: 0.01,
                friction: 0.15,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                groupCells: 3,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: false,
                contain: true,
                wrapAround: true,
                prevNextButtons: false,
                pageDots: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3
            });

        });
    }

    $.carouselReviews = function () {

        var $el = $('.js-reviews-carousel');

        if (!$el.length || $('.js-reviews-carousel .carousel-item').length <= 2) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                cellSelector: '.carousel-item',
                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                lazyLoad: 2,
                imagesLoaded: true,
                selectedAttraction: 0.01,
                friction: 0.15,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $elAmount = $(this).find('.flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next());

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                groupCells: 3,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: false,
                contain: true,
                wrapAround: true,
                prevNextButtons: false,
                pageDots: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3
            });

            $(this).find('.flickity-button').appendTo($(this).next());

        });
    }

    $.carouselGoods = function () {

        var $el = $('.js-goods-carousel');

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                cellSelector: '.carousel-item',
                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                lazyLoad: 4,
                imagesLoaded: true,
                selectedAttraction: 0.01,
                friction: 0.15,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $elAmount = $(this).find('.flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next());

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                groupCells: 3,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: false,
                contain: true,
                wrapAround: true,
                prevNextButtons: false,
                pageDots: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3
            });

            $(this).find('.flickity-button').appendTo($(this).next());

        });
    }

    // Функция сборки элементов кастомной навигации

    function createCustomNav(index, nav) {

        for (var i = 0; i < index; i++) {
            nav.append('<div class="carousel-nav_dot"><span class="dot"></span></div>');
        }

    }

    // Всплывающая галерея
    $.galleryPopup = function (el) {

        var $el = el;

        if (!$el.length) return;

        $el.magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Загрузка #%curr%...',
            tClose: 'Закрыть',
            mainClass: 'mfp-gallery',
            gallery: {
                arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%"><svg style="width: 14px; height: 24px;"><use xlink:href="#icon-angle-right"></use></svg></button>',
                enabled: true,
                tCounter: '<span class="mfp-counter1">%curr% из %total%</span>',
                navigateByImgClick: true
            },
            image: {
                tError: '<a href="%url%">Изображение не найдено',
            },
            callbacks: {
                open: function () {
                    $('.mfp-arrow').appendTo($('.mfp-content'));
                    $('.mfp-gallery').find('.mfp-close').html('<svg style="width: 30px; height: 30px;"><use xlink:href="#icon-close-btn"></use></svg>');
                }
            }
        });
    };

    // создаем блоки для подменю в моб.меню категорий

    $('.categories-menu').clone().removeClass('desktop').appendTo('.categories_mob-menu');

    // формируем заголовки в моб.меню

    var arrCatTitle = [];

    $('.categories-menu.desktop .has-child > a').each(function (index) {

        arrCatTitle[index] = $(this).html();

    });

    $('#categories_mob-menu .child-item .child-item_layout').each(function (i) {

        $(this).prepend('<p class="child-item_category">' + arrCatTitle[i] + '</p>');

    });

    // Инициализация мобильного меню категорий

    $.menu = function () {
        $('#categories_mob-menu').mmenu({
            extensions: ["theme-white", "fx-menu-fade"],
            navbar: {
                title: ''
            }
        });
    }

    $.menu();

    // удаляем ненужный текст и вставляем свой

    $('.categories_mob-menu .mm-navbar__title').html('НАЗАД');


    /* Устанавливаем дефолтные настройки валиации */

    $.validator.messages.required = 'Заполните поле';

    $.validator.addClassRules({
        jsRequired: {
            required: true
        },
        jsRequiredEmail: {
            required: true,
            email: true
        }
    });

    //validator

    $.Valid = function (el) {

        var $el = el;
        if (!$el.length) return;

        var validator = $el.validate({

            focusInvalid: false, // убираем автоматический фокус

            errorElement: 'span',

            name: "required",
            phone: "required",
            message: "message",
            individual_phone: "required",
            password_confirmation: "required",
            message: "required",
            city: "required",
            address: "required",

            rules: {
                login: {
                    required: true,
                    minlength: 3
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "input[name=password]"
                },
                checkbox: {
                    required: true,
                    minlength: 1
                }
            },

            messages: {
                email: {
                    required: 'Заполните поле',
                    email: 'Неверно заполнено поле'
                },
                checkbox: {
                    required: ''
                },
                name: {
                    required: 'Заполните поле'
                },
                phone: {
                    required: 'Введите номер телефона'
                },
                message: {
                    required: 'Напишите отзыв'
                },
                individual_phone: {
                    required: 'Введите номер телефона'
                },
                message: {
                    required: 'Ваше сообщение'
                },
                city: {
                    required: 'Ваш город'
                },
                address: {
                    required: 'Ваш адрес'
                },
                password: {
                    required: 'Введите пароль',
                    minlength: 'Минимальное количество символов - 6'
                },
                password_confirmation: {
                    required: 'Подтвердите пароль',
                    equalTo: 'Пароли не совпадают'
                },
                login: {
                    required: 'Введите логин',
                    minlength: 'Минимальное количество символов - 3'
                },
                jsRequiredEmail: {
                    required: true,
                    email: true
                }
            },

            highlight: function (input, errorClass, validClass) {
                $(input).addClass(errorClass).removeClass(validClass);
                $(input).parent().addClass(errorClass);
            },

            unhighlight: function (input, errorClass, validClass) {
                $(input).removeClass(errorClass).addClass(validClass);
                $(input).parent().removeClass(errorClass);
            },

            showErrors: function () {
                this.defaultShowErrors();

                var result = 0;
                var amountInput = $('.checkout-block.is-open .tab-container_item.open .required').length;

                $('.checkout-block.is-open .tab-container_item.open .required').each(function () {
                    if ($(this).hasClass('valid')) {
                        result += 1;
                    } else {
                        return false;
                    }
                });

                if (result == amountInput) {

                    var ths = $('.checkout-block.is-open');
                    ths.next().removeClass('is-close').addClass('is-open');
                    ths.removeClass('is-open');

                    (function () {
                        $('.checkout-block.is-open').find('span.error').remove();
                        $('.checkout-block.is-open').find('.error').removeClass('error');
                    })();
                }

                return false;
            }
        });

    }

    // Переключает вкладки (страница товара, сертификаты, контакты)

    $(document).on('click touch', '.js-nav-tabs .tab', function () {

        var index = $(this).index();
        var ths = $(this);
        var tabs = $(this).parent().find('.active');
        var containerItems = $(this).parent().next().find('.tab-container_item');

        //        if ($(this).parent().next().hasClass('alphabet-basic')) {
        //            containerItems = $(this).parent().next().next().find('.tab-container_item');
        //        }
        //
        //        if ($(this).parent().hasClass('flickity-slider')) {
        //            containerItems = $(this).parent().parent().parent().next().find('.tab-container_item');
        //        }

        // переопределяем переменную, если это на странице Контакты
        //        if ($(this).parent().parent().hasClass('magazine-catalog_heading')) {
        //            containerItems = $(this).parent().parent().next().find('.tab-container_item');
        //        }

        if (ths.hasClass('active')) {
            return false;
        }

        tabs.removeClass('active');
        containerItems.removeClass('open');
        ths.addClass('active');

        containerItems.each(function () {

            if ($(this).index() == index) {

                $(this).addClass('open');

                // если в табах есть карусель, обновляем высоту слайдера и навигации
                $(this).find('.js-carousel-gallery').flickity('resize');

                $(this).find('.js-carousel-gallery').next().flickity('resize');
            }

        });

    });

    // Вкладки для блока Галереи

    $('.js-text-dynamic').html($('.js-nav-gallery .tab.active').html());

    $(document).on('click touch', '.js-nav-gallery .tab', function () {
        $('.js-text-dynamic').html($('.js-nav-gallery .tab.active').html());
    });


    /* *** */

    $(document).on('click touch', '.js-search-toggle', function () {
            $(this).parent().toggleClass('active is-closes-out');
        }) // событие клика по веб-документу. Скрываем блок, если клик не по нему
        .on('click touch', function (e) {
            var elem = $(".is-closes-out");
            if (!elem.is(e.target) && elem.has(e.target).length === 0) {
                elem.closest('.active').removeClass('active').end().removeClass('is-closes-out'); // скрываем его
            }
        })
        .on('click touch', '.js-menu-toggle', function () {
            // открываем/закрываем меню с ссылками на страницы
            $('.js-menu').toggleClass('is-open');
        })
        .on('click touch', '.js-product-add-favorite', function () {
            $(this).toggleClass('is-added');
        })
        .on('click touch', '.js-toggle-next', function () {
            $(this).next().toggleClass('is-active');
        })
        .on('click touch', '.js-gallery-toggle', function () {
            let elemNext = $(this).next();

            $(this).toggleClass('is-active').next().toggleClass('is-active');


            setTimeout(function () {
                elemNext.toggleClass('js-is-open');
            }, 100);
        })
        .on('click touch', '.js-slide-toggle-next', function () {

            var ths = $(this);

            $(this).toggleClass('is-active').next().slideToggle(350);

            setTimeout(function () {
                ths.next().toggleClass('is-active');
            }, 360);
        })
        .on('click touch', '.js-filter-show-all', function () {
            $(this).parents('.filter-item_settings').addClass('show-all');
        })
        .on('click touch', '.js-footer-nav-toggle', function () {

            $(this).toggleClass('is-active').next().slideToggle('335');

        })
        .on('click touch', '.js-filter-toggle', function () {
            // открываем/закрываем фильтр в моб.версиях
            $('.filter-wrap').toggleClass('is-active');

        })
        .on('click touch', '.js-dropdown-toggle', function () {

            var ths = $(this);

            ths.parent().parent().find('.dropdown').toggleClass('is-active');

            setTimeout(function () {
                ths.parent().parent().find('.dropdown').toggleClass('js-is-open');
            }, 100);

        })
        .on('click touch', '.js-product-choose', function (e) {
            
            e.preventDefault();
        
            if ($(this).hasClass('is-active')) return false;
        
            $(this).parent().find('.is-active').removeClass('is-active');
            
            $(this).addClass('is-active');

        });

    // убираем прозрачность элемента, после загрузки страницы

    $(window).on('ready load', function () {

        $('.js-remove-opacity').animate({
            opacity: '1'
        }, 400);

    });

    // прячем табы при загрузке и ресайзе viewport

    $(window).on('ready load resize', function () {

        // показываем теги после отработки скрипта
        $('.tags').animate({
            opacity: "1"
        }, 350);

        var tagsItem = $('.tags-list_inner a'),
            tagsLength = 0,
            tagsParentWidth = $('.tags-list').width() - 60; // ширина родительского блока, минус ширина, для того чтобы скрывать элементы раньше

        tagsItem.each(function () {

            // считаем ширину элемента + его padding-right 

            tagsLength += 18 + $(this).width();

            /* проверяем ширину всех элементов с шириной родительского блока,
               и вставляем в выпадающий блок,
               если ширина всех элементов больше ширины родительского блока */

            if (tagsLength > tagsParentWidth) {
                $(this).appendTo('.tags-dropdown-list');
            }
        });

    });

    // скрываем блок, если клик не по нему

    $(document).on('click touch', function (e) {
        var elem = $(".js-is-open");

        if (!elem.is(e.target) && elem.has(e.target).length === 0) {
            setTimeout(function () {
                elem.closest('.is-active').removeClass('is-active').end().removeClass('js-is-open');
                elem.prev().removeClass('is-active').end().removeClass('js-is-open');
            }, 150);
        }
    });
    
    // Карточка товара: заполняем блоки в мобильной версии
    
    $('.product-layout .tags').clone().appendTo('.product-mobile_block');
    $('.product-layout .product-benefits').clone().appendTo('.product-mobile_block');
    $('.product-layout .statistics').clone().appendTo('.product-mobile_block');
    $('.product-layout .product-composition > *').clone().appendTo('.product-mobile_composition');


}(jQuery));
