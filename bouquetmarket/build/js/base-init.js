$(function() {
	
    
    // Если нет поддержки тега <svg>
    if (!Modernizr.inlinesvg) {
        
        $('[data-png]').each(function(){
        
            var src = $(this).data('png');
            
            $(this).before($('<img src="' + src + '" alt="">')).remove();
            
        });
        
    }
    
    
    // Если нет поддержки картинки image.svg
    if (!Modernizr.svgasimg) {
            
        $('img[src $= ".svg"]').each(function(){
        
            var src = $(this).attr('src');
            
            $(this).attr('src', src.replace('.svg', '.png'));
            
        });
        
    }
    
    $('form.js-form-validation').each(function () {
        $.Valid($(this));
    });
    
	// Carousels init
    $.carouselReviewCard();
    $.carouselGallery();
    $.carouselProduct();
    $.carouselReviews();
    $.carouselGoods();
    
    // gallery popup
    $.galleryPopup(
        $('.js-gallery-popup')
    );
    
    // убираем пробелы
    $.noSpace();
    
});