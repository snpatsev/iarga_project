/*!
 * Flickity fullscreen v1.1.1
 * Enable fullscreen view for Flickity
 */

/*jshint browser: true, undef: true, unused: true, strict: true*/

/* ! Have CUSTOM SETTINGS ! */

(function (window, factory) {
    // universal module definition
    /*jshint strict: false */
    /*globals define, module, require */
    if (typeof define == 'function' && define.amd) {
        // AMD
        define([
      'flickity/js/index',
    ], factory);
    } else if (typeof module == 'object' && module.exports) {
        // CommonJS
        module.exports = factory(
            require('flickity')
        );
    } else {
        // browser global
        factory(
            window.Flickity
        );
    }

}(window, function factory(Flickity) {

    'use strict';

    /* Добавляем русскоязычные title */

    var ruNameView = 'Открыть';
    var ruNameExit = 'Закрыть';

    Flickity.createMethods.push('_createFullscreen');
    var proto = Flickity.prototype;


    proto._createFullscreen = function () {
        this.isFullscreen = false;

        if (!this.options.fullscreen) {
            return;
        }
        // buttons
        this.viewFullscreenButton = new FullscreenButton('view', this);
        this.exitFullscreenButton = new FullscreenButton('exit', this);

        this.on('activate', this._changeFullscreenActive);
        this.on('deactivate', this._changeFullscreenActive);
    };

    // ----- activation ----- //

    proto._changeFullscreenActive = function () {
        var childMethod = this.isActive ? 'appendChild' : 'removeChild';
        this.element[childMethod](this.viewFullscreenButton.element);
        this.element[childMethod](this.exitFullscreenButton.element);
        // activate or deactivate buttons
        var activeMethod = this.isActive ? 'activate' : 'deactivate';
        this.viewFullscreenButton[activeMethod]();
        this.exitFullscreenButton[activeMethod]();
    };

    // ----- view, exit, toggle ----- //

    proto.viewFullscreen = function () {
        this._changeFullscreen(true);
        this.focus();
    };

    proto.exitFullscreen = function () {
        this._changeFullscreen(false);
    };

    proto._changeFullscreen = function (isView) {
        if (this.isFullscreen == isView) {
            return;
        }
        this.isFullscreen = isView;
        var classMethod = isView ? 'add' : 'remove';
        document.documentElement.classList[classMethod]('is-flickity-fullscreen');
        this.element.classList[classMethod]('is-fullscreen');
        this.resize();
        // HACK extra reposition on fullscreen for images
        if (this.isFullscreen) {
            this.reposition();
        }
        this.dispatchEvent('fullscreenChange', null, [isView]);
    };

    proto.toggleFullscreen = function () {
        this._changeFullscreen(!this.isFullscreen);
    };

    // ----- setGallerySize ----- //

    // overwrite so fullscreen cells are full height
    var setGallerySize = proto.setGallerySize;
    proto.setGallerySize = function () {
        if (!this.options.setGallerySize) {
            return;
        }
        if (this.isFullscreen) {
            // remove height style on fullscreen
            this.viewport.style.height = '';
        } else {
            // otherwise, do normal
            setGallerySize.call(this);
        }
    };

    // ----- keyboard ----- //

    // ESC key closes full screen
    Flickity.keyboardHandlers[27] = function () {
        this.exitFullscreen();
    };

    // ----- FullscreenButton ----- //

    function FullscreenButton(name, flickity) {
        this.name = name;
        this.createButton();
        this.createIcon();
        // events
        // trigger viewFullscreen or exitFullscreen on click
        this.onClick = function () {
            flickity[name + 'Fullscreen']();
        };
        this.clickHandler = this.onClick.bind(this);
    }

    FullscreenButton.prototype.createButton = function () {
        var element = this.element = document.createElement('button');
        element.className = 'flickity-button flickity-fullscreen-button ' +
            'flickity-fullscreen-button-' + this.name;
        // prevent button from submitting form
        element.setAttribute('type', 'button');
        // set label

        /* Ставим условие для вывода title */
        if (this.name == 'view') {
            var label = capitalize(ruNameView + ' полноэкранный режим');
            element.setAttribute('aria-label', label);
            element.title = label;
        } else {
            var label = capitalize(ruNameExit + ' полноэкранный режим');
            element.setAttribute('aria-label', label);
            element.title = label;
        }
        /* *** */
    };

    function capitalize(text) {
        return text[0].toUpperCase() + text.slice(1);
    }

    var svgURI = 'http://www.w3.org/2000/svg';

    /* Добавил кастомные иконки */

    var pathDirections = {
        view: 'M19.68,18.13l-5.552-5.179c1.05-1.319,1.711-2.963,1.739-4.779c0.066-4.375-3.424-7.976-7.799-8.043C3.693,0.06,0.091,3.552,0.024,7.927c-0.067,4.375,3.425,7.974,7.799,8.042c1.967,0.03,3.753-0.685,5.156-1.848l5.585,5.205c0.16,0.148,0.369,0.228,0.588,0.221c0.217-0.009,0.421-0.1,0.568-0.258C20.028,18.958,20.011,18.438,19.68,18.13z M7.849,14.332c-3.47-0.054-6.239-2.911-6.186-6.379c0.054-3.471,2.909-6.239,6.379-6.186c3.468,0.054,6.239,2.91,6.186,6.38C14.174,11.614,11.316,14.385,7.849,14.332z',
        exit: 'M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z',
    };

    FullscreenButton.prototype.createIcon = function () {
        var svg = document.createElementNS(svgURI, 'svg');
        svg.setAttribute('class', 'flickity-button-icon');

        /* Ставим условие для viewBox-a */
        if (this.name == 'view') {
            svg.setAttribute('viewBox', '0 0 21 21');
        } else {
            svg.setAttribute('viewBox', '0 0 26 26');
            svg.setAttribute('style','width: 0.75rem; height: 0.75rem;');
        }
        /* *** */

        // path & direction
        var path = document.createElementNS(svgURI, 'path');
        var direction = pathDirections[this.name];
        path.setAttribute('d', direction);
        // put it together
        svg.appendChild(path);
        this.element.appendChild(svg);
    };

    FullscreenButton.prototype.activate = function () {
        this.element.addEventListener('click', this.clickHandler);
    };

    FullscreenButton.prototype.deactivate = function () {
        this.element.removeEventListener('click', this.clickHandler);
    };

    Flickity.FullscreenButton = FullscreenButton;

    // ----- fin ----- //

    return Flickity;

}));
