$.noSpace = function (){

    var list = document.querySelectorAll('.js-nospace'),
        listChild,
        i,
        j;

    for (i = list.length; i--;) {

        listChild = list[i].childNodes;

        for (j = listChild.length; j--;) {
            if (listChild[j].nodeValue !== null) {
                listChild[j].textContent = '';
            }
        }
    }
}
