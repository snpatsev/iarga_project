// Загружаем шрифт асинхронно
WebFontConfig = {
    custom: {
        families: ['Helvetica', 'GretaTextPro'],
        urls: ['css/fonts.css']
    },
    // если с гугла, то подклюаем как так
    /*
    google: { families: ['Open Sans:300,400,400italic,600,700'] }
    */
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);