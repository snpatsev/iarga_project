$(function () {


    // Если нет поддержки тега <svg>
    if (!Modernizr.inlinesvg) {

        $('[data-png]').each(function () {

            var src = $(this).data('png');

            $(this).before($('<img src="' + src + '" alt="">')).remove();

        });

    }


    // Если нет поддержки картинки image.svg
    if (!Modernizr.svgasimg) {

        $('img[src $= ".svg"]').each(function () {

            var src = $(this).attr('src');

            $(this).attr('src', src.replace('.svg', '.png'));

        });

    }

    //Кастомный скроллбар

    if ($('.js-scrollbar').length) {
        ps = new PerfectScrollbar('.js-scrollbar', {
            wheelSpeed: 1
        });
    }
    

    //Макса для номера телефона
    $.PhoneMask();

    $('form.with-validation').each(function () {
        $.Valid($(this));
    });
    
    $.carouselMain();
    $.carouselNavCategory();
    $.carouselProduct();
    $.carouselProductBenefit();
    $.carouselProductBlock();
    $.carouselProductAlt();
    $.carouselProductAltSmall();
    $.carouselPartners();
    $.carouselBenefit();
    $.carouselLastnews();
    $.carouselNavTabs();
    $.carouselNewsSpecific();
    $.carouselOtherNews();
    $.carouselCertificate();
    $.carouselMagazine();

    $.carouselPopup(
        $('.js-carousel-popup')
    );

    $.ratingSelect();
    
    // Popup init
    $.popupAjax(
        $('.js-popup')
    );

    $.popupNoAjax(
        $('.js-popup-no-ajax')
    );

    /* Remove space between elements */

    $.noSpace();
});
