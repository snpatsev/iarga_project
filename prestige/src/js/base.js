(function ($) {

    $(function () {

        // Если нет поддержки тега <svg>

        if (!Modernizr.inlinesvg) {

            $('[data-png]').each(function () {

                var src = $(this).data('png');

                $(this).before($('<img src="' + src + '" alt="">')).remove();

            });

        }

        // Если нет поддержки картинки image.svg

        if (!Modernizr.svgasimg) {

            $('img[src $= ".svg"]').each(function () {

                var src = $(this).attr('src');

                $(this).attr('src', src.replace('.svg', '.png'));

            });

        }
    });

    // Попап
    $.popupAjax = function (el) {

        var $el = el;

        if (!$el.length) return;

        $el.magnificPopup({
            closeMarkup: '<span class="mfp-close close"><svg style="width: 1.5rem; height: 1.5rem;"><use xlink:href="#icon-close"></use></svg></span>',
            type: 'ajax',
            overflowY: 'scroll',
            preloader: false,
            removalDelay: 300,
            tClose: 'Закрыть',
            tLoading: 'Загрузка...',
            callbacks: {
                ajaxContentAdded: function () {
                    //Макса для номера телефона
                    $.PhoneMask();
                    $("form.with-validation").each(function () {
                        $.Valid($(this));
                    });
                    $.countDimensions();
                }
            }

        });
    };

    //Макса для номера телефона
    $.PhoneMask = function () {
        var $el = $('input.jsPhoneMask');

        if (!$el.length) return;

        $el.mask("+7 (999) 999-99-99", {
            placeholder: "+7 (XXX) XXX-XX-XX"
        });
    }

    // Попап без ajax

    $.popupNoAjax = function (el) {
        var $el = el;

        if (!$el.length) return;

        $el.magnificPopup();
    }

    /* Устанавливаем дефолтные настройки валиации */

    $.validator.messages.required = 'Заполните поле';

    $.validator.addClassRules({
        jsRequired: {
            required: true
        },
        jsRequiredEmail: {
            required: true,
            email: true
        }
    });


    //validator
    $.Valid = function (el) {

        var $el = el;
        if (!$el.length) return;

        var validator = $el.validate({

            focusInvalid: false, // убираем автоматический фокус
            onfocusout: false,
            errorElement: 'span',

            name: "required",
            phone: "required",
            message: "message",
            individual_phone: "required",
            password_confirmation: "required",
            message: "required",
            city: "required",
            address: "required",

            rules: {
                login: {
                    required: true,
                    minlength: 3
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    equalTo: "input[name=password]"
                },
                checkbox: {
                    required: true,
                    minlength: 1
                }
            },

            messages: {
                email: {
                    required: 'Заполните поле',
                    email: 'Неверно заполнено поле'
                },
                checkbox: {
                    required: ''
                },
                name: {
                    required: 'Заполните поле'
                },
                phone: {
                    required: 'Введите номер телефона'
                },
                message: {
                    required: 'Напишите отзыв'
                },
                individual_phone: {
                    required: 'Введите номер телефона'
                },
                message: {
                    required: 'Ваше сообщение'
                },
                city: {
                    required: 'Ваш город'
                },
                address: {
                    required: 'Ваш адрес'
                },
                password: {
                    required: 'Введите пароль',
                    minlength: 'Минимальное количество символов - 6'
                },
                password_confirmation: {
                    required: 'Подтвердите пароль',
                    equalTo: 'Пароли не совпадают'
                },
                login: {
                    required: 'Введите логин',
                    minlength: 'Минимальное количество символов - 3'
                },
                jsRequiredEmail: {
                    required: true,
                    email: true
                }
            },

            highlight: function (input, errorClass, validClass) {
                $(input).addClass(errorClass).removeClass(validClass);
                $(input).parent().addClass(errorClass);
            },

            unhighlight: function (input, errorClass, validClass) {
                $(input).removeClass(errorClass).addClass(validClass);
                $(input).parent().removeClass(errorClass);
            },

            showErrors: function () {
                this.defaultShowErrors();

                var result = 0;
                var amountInput = $('.checkout-block.is-open .tab-container_item.open .required').length;

                $('.checkout-block.is-open .tab-container_item.open .required').each(function () {
                    if ($(this).hasClass('valid')) {
                        result += 1;
                    } else {
                        return false;
                    }
                });

                if (result == amountInput) {

                    var ths = $('.checkout-block.is-open');
                    ths.next().removeClass('is-close').addClass('is-open');
                    ths.removeClass('is-open');

                    (function () {
                        $('.checkout-block.is-open').find('span.error').remove();
                        $('.checkout-block.is-open').find('.error').removeClass('error');
                    })();
                }

                return false;
            }
        });
    }

    $(document).on("click touch", ".js-checkout-valid", function () {
        $(this).parents("form").valid();
    });

    // Выводим имя для input type=file
    $('input#file').on('change', function () {

        if (!$(this).val().length) return;

        var value = $(this).val().split("\\");
        var name = value[value.length - 1];

        $('.js-for-file .text').html(name);

    });

    $(document).on('click touch', '.js-jurist-allow', function () {
            $('.js-jurist-payment').addClass('is-allowed');
        })
        .on('click touch', '.js-jurist-not-allow', function () {
            $('.js-jurist-payment').removeClass('is-allowed');
        });

    // Карусель на главной
    $.carouselMain = function () {

        var $el = $('.js-carousel-main');
        if (!$el.length) return;

        $('html').addClass('overflow-x-hidden');

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 1,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: 5500,
            prevNextButtons: true,
            pageDots: false,
            wrapAround: true,
            setGallerySize: false,
            arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
        });

        $('.js-nav-main').flickity({
            asNavFor: '.js-carousel-main',
            lazyLoad: 3,
            cellAlign: 'left',
            imagesLoaded: true,
            contain: true,
            groupCells: 5,
            //            wrapAround: true,
            prevNextButtons: false,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            pageDots: false
        });

        $('.js-carousel-main .flickity-button').appendTo('.js-nav-main');

    }

    /* Карточка товара */
    $.carouselProduct = function () {

        var $el = $('.js-carousel-product');
        if (!$el.length) return;
        var pageWidth = $('html, body').width();

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 1,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            prevNextButtons: true,
            pageDots: true,
            wrapAround: true,
            adaptiveHeight: true,
            imagesLoaded: true,
            cellSelector: '.carousel-item'
        });

        $('.js-nav-product').flickity({
            asNavFor: '.js-carousel-product',
            lazyLoad: 3,
            cellAlign: 'left',
            imagesLoaded: true,
            groupCells: 2,
            draggable: true,
            contain: pageWidth > 479 ? true : false,
            prevNextButtons: false,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            pageDots: false
        });
    }

    $.carouselNavCategory = function () {

        var $el = $('.js-carousel-nav-category');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: 5500,
            prevNextButtons: false,
            pageDots: false,
            freeScroll: true,
            wrapAround: true
        });
    }

    // Карусель блока Выгоды на главной

    $.carouselBenefit = function () {


        var $el = $('.js-carousel-benefit');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'center',
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: false,
            prevNextButtons: true,
            pageDots: false,
            freeScroll: false,
            wrapAround: true,
            watchCSS: true,
            arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
        });
    }

    $.carouselProductBlock = function () {

        var $el = $('.js-carousel-product-block'),
            $elAmount;

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                //                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $elAmount = $('.js-carousel-product-block .flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next());

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                cellAlign: 'left',
                imagesLoaded: true,
                groupCells: 5,
                contain: true,
                prevNextButtons: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                pageDots: false
            });

            $(this).find('.flickity-button').appendTo($(this).next());
        });
    }

    $.carouselProductAlt = function () {

        var $el = $('.js-carousel-alt-block');

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'center',
                lazyLoad: 1,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                //                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                watchCSS: true,
                arrowShape: 'M7.87693 0.479617L7.88054 0.484506C8.20594 0.185777 8.63337 0 9.10816 0C10.1136 0 10.9296 0.815997 10.9296 1.82145C10.9296 2.35332 10.6989 2.82685 10.3358 3.15961L10.3395 3.1645L4.51819 8.49997L10.3395 13.8368L10.3359 13.8416C10.699 14.1731 10.9297 14.6467 10.9297 15.1785C10.9297 16.184 10.1137 17 9.10823 17C8.63343 17 8.206 16.8142 7.88061 16.5168L7.87699 16.5216L0.591273 9.84301C0.216035 9.49692 0.0011158 9.0112 0.0011158 8.50003C0.0011158 7.98886 0.216035 7.50308 0.591273 7.15705L7.87693 0.479617Z'
            });
        });
    }

    $.carouselProductAltSmall = function () {

        var $el = $('.js-carousel-alt-block_small');

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                lazyLoad: 3,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                //                autoPlay: 5500,
                prevNextButtons: false,
                pageDots: false,
                wrapAround: true,
                watchCSS: true
            });
        });
    }

    $.carouselPartners = function () {

        var $el = $('.js-carousel-partners');

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                lazyLoad: 3,
                imagesLoaded: true,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                //                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                arrowShape: 'M7.87693 0.479617L7.88054 0.484506C8.20594 0.185777 8.63337 0 9.10816 0C10.1136 0 10.9296 0.815997 10.9296 1.82145C10.9296 2.35332 10.6989 2.82685 10.3358 3.15961L10.3395 3.1645L4.51819 8.49997L10.3395 13.8368L10.3359 13.8416C10.699 14.1731 10.9297 14.6467 10.9297 15.1785C10.9297 16.184 10.1137 17 9.10823 17C8.63343 17 8.206 16.8142 7.88061 16.5168L7.87699 16.5216L0.591273 9.84301C0.216035 9.49692 0.0011158 9.0112 0.0011158 8.50003C0.0011158 7.98886 0.216035 7.50308 0.591273 7.15705L7.87693 0.479617Z'
            });
        });
    }

    $.carouselLastnews = function () {

        var $el = $('.js-carousel-lastnews'),
            $elAmount;

        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                lazyLoad: 3,
                imagesLoaded: true,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                //                autoPlay: 5500,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                watchCSS: true,
                arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
            });

            $elAmount = $('.js-carousel-lastnews .flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next());

            $(this).next().flickity({
                asNavFor: this,
                lazyLoad: 3,
                cellAlign: 'left',
                imagesLoaded: true,
                //                wrapAround: true,
                freeScroll: true,
                groupCells: 5,
                contain: true,
                prevNextButtons: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                pageDots: false
            });

            $(this).find('.flickity-button').appendTo($(this).next());
        });
    }

    $.carouselProductBenefit = function () {

        var $el = $('.js-carousel-product-benefit');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: false,
            prevNextButtons: false,
            pageDots: false,
            watchCSS: true,
            adaptiveHeight: true,
            imagesLoaded: true
        });
    }

    $.carouselNavTabs = function () {

        var $el = $('.js-carousel-nav-tabs');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: false,
            prevNextButtons: true,
            pageDots: false,
            freeScroll: true,
            watchCSS: true,
            adaptiveHeight: true,
            arrowShape: 'M7.87595 0.479617L7.87957 0.484506C8.20496 0.185777 8.63239 0 9.10719 0C10.1126 0 10.9286 0.815997 10.9286 1.82145C10.9286 2.35332 10.6979 2.82685 10.3349 3.15961L10.3385 3.1645L4.51722 8.49997L10.3386 13.8368L10.3349 13.8416C10.698 14.1731 10.9287 14.6467 10.9287 15.1785C10.9287 16.184 10.1127 17 9.10725 17C8.63246 17 8.20503 16.8142 7.87963 16.5168L7.87601 16.5216L0.590297 9.84301C0.215058 9.49692 0.000139236 9.0112 0.000139236 8.50003C0.000139236 7.98886 0.215058 7.50308 0.590297 7.15705L7.87595 0.479617Z'
        });
    }

    $.carouselNewsSpecific = function () {

        var $el = $('.js-carousel-news-specific');
        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 1,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            prevNextButtons: true,
            pageDots: false,
            wrapAround: true,
            adaptiveHeight: true,
            imagesLoaded: true,
            arrowShape: 'M7.87595 16.5204L7.87957 16.5155C8.20496 16.8142 8.63239 17 9.10719 17C10.1126 17 10.9286 16.184 10.9286 15.1785C10.9286 14.6467 10.6979 14.1732 10.3349 13.8404L10.3385 13.8355L4.51722 8.50003L10.3386 3.16323L10.3349 3.1584C10.698 2.82691 10.9287 2.35333 10.9287 1.82146C10.9287 0.815998 10.1127 7.13371e-08 9.10725 1.59237e-07C8.63245 2.00745e-07 8.20503 0.185778 7.87963 0.483237L7.87601 0.478413L0.590296 7.15699C0.215057 7.50308 0.000138449 7.9888 0.000138493 8.49997C0.000138538 9.01114 0.215058 9.49692 0.590296 9.84295L7.87595 16.5204Z'
        });

        $el.next().flickity({
            asNavFor: '.js-carousel-news-specific',
            cellAlign: 'left',
            imagesLoaded: true,
            lazyLoad: 3,
            groupCells: 2,
            draggable: true,
            contain: true,
            prevNextButtons: false,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            pageDots: false
        });

        $el.find('.flickity-button').appendTo($el.next());
    }

    $.carouselOtherNews = function () {

        var $el = $('.js-other-news');
        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 3,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            prevNextButtons: false,
            pageDots: false,
            autoPlay: 4500,
            wrapAround: true,
            imagesLoaded: true,
            watchCSS: true,
            arrowShape: 'M7.87595 16.5204L7.87957 16.5155C8.20496 16.8142 8.63239 17 9.10719 17C10.1126 17 10.9286 16.184 10.9286 15.1785C10.9286 14.6467 10.6979 14.1732 10.3349 13.8404L10.3385 13.8355L4.51722 8.50003L10.3386 3.16323L10.3349 3.1584C10.698 2.82691 10.9287 2.35333 10.9287 1.82146C10.9287 0.815998 10.1127 7.13371e-08 9.10725 1.59237e-07C8.63245 2.00745e-07 8.20503 0.185778 7.87963 0.483237L7.87601 0.478413L0.590296 7.15699C0.215057 7.50308 0.000138449 7.9888 0.000138493 8.49997C0.000138538 9.01114 0.215058 9.49692 0.590296 9.84295L7.87595 16.5204Z'
        });
    }

    $.carouselCertificate = function () {

        var $el = $('.js-carousel-certificate'),
            $elAmount;

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 4,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            prevNextButtons: true,
            pageDots: false,
            wrapAround: true,
            adaptiveHeight: true,
            imagesLoaded: true,
            arrowShape: 'M7.87595 16.5204L7.87957 16.5155C8.20496 16.8142 8.63239 17 9.10719 17C10.1126 17 10.9286 16.184 10.9286 15.1785C10.9286 14.6467 10.6979 14.1732 10.3349 13.8404L10.3385 13.8355L4.51722 8.50003L10.3386 3.16323L10.3349 3.1584C10.698 2.82691 10.9287 2.35333 10.9287 1.82146C10.9287 0.815998 10.1127 7.13371e-08 9.10725 1.59237e-07C8.63245 2.00745e-07 8.20503 0.185778 7.87963 0.483237L7.87601 0.478413L0.590296 7.15699C0.215057 7.50308 0.000138449 7.9888 0.000138493 8.49997C0.000138538 9.01114 0.215058 9.49692 0.590296 9.84295L7.87595 16.5204Z'
        });

        $elAmount = $('.js-carousel-certificate .flickity-slider > *').length;

        createCustomNav($elAmount, $el.next());

        $el.next().flickity({
            asNavFor: '.js-carousel-certificate',
            cellAlign: 'center',
            imagesLoaded: true,
            lazyLoad: 3,
            groupCells: 5,
            draggable: true,
            contain: true,
            prevNextButtons: false,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            pageDots: false
        });

        $el.find('.flickity-button').appendTo($el.next());

    }

    $.carouselMagazine = function () {

        var $el = $('.js-carousel-magazine'),
            $elAmount;
        if (!$el.length) return;

        $el.each(function () {

            $(this).flickity({
                cellAlign: 'left',
                lazyLoad: 1,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                prevNextButtons: true,
                pageDots: false,
                wrapAround: true,
                adaptiveHeight: true,
                imagesLoaded: true,
                arrowShape: 'M7.87595 16.5204L7.87957 16.5155C8.20496 16.8142 8.63239 17 9.10719 17C10.1126 17 10.9286 16.184 10.9286 15.1785C10.9286 14.6467 10.6979 14.1732 10.3349 13.8404L10.3385 13.8355L4.51722 8.50003L10.3386 3.16323L10.3349 3.1584C10.698 2.82691 10.9287 2.35333 10.9287 1.82146C10.9287 0.815998 10.1127 7.13371e-08 9.10725 1.59237e-07C8.63245 2.00745e-07 8.20503 0.185778 7.87963 0.483237L7.87601 0.478413L0.590296 7.15699C0.215057 7.50308 0.000138449 7.9888 0.000138493 8.49997C0.000138538 9.01114 0.215058 9.49692 0.590296 9.84295L7.87595 16.5204Z'
            });

            $(this).next().flickity({
                asNavFor: '.js-carousel-magazine',
                lazyLoad: 5,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: true,
                contain: true,
                prevNextButtons: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                pageDots: false
            });

            $elAmount = $('.js-carousel-magazine .flickity-slider > *').length;

            createCustomNav($elAmount, $(this).next().next());

            $(this).next().next().flickity({
                asNavFor: '.js-carousel-magazine',
                lazyLoad: 3,
                groupCells: 5,
                cellAlign: 'left',
                imagesLoaded: true,
                draggable: true,
                contain: true,
                prevNextButtons: false,
                dragThreshold: Modernizr.touchevents ? 10 : 3,
                pageDots: false
            });


            $(this).find('.flickity-button').appendTo($(this).next().next());

        });
    }

    $.carouselPopup = function (el) {

        var $el = el;

        if (!$el.length) return;

        $el.each(function () {

            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Загрузка #%curr%...',
                tClose: 'Закрыть',
                mainClass: 'mfp-certificate',
                gallery: {
                    arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%"><svg style="width: 20px; height: 24px;"><use xlink:href="#icon-angle"></use></svg></button>',
                    enabled: true,
                    navigateByImgClick: true
                },
                image: {
                    tError: '<a href="%url%">Изображение не найдено',
                },
                callbacks: {
                    open: function () {
                        $('.mfp-arrow').appendTo($('.mfp-content'));
                        $('.mfp-certificate').find('.mfp-close').html('<svg style="width: 1.5rem; height: 1.5rem;"><use xlink:href="#icon-close"></use></svg>');
                    }
                }
            });

        });
    };

    // Функция сборки элементов кастомной навигации

    function createCustomNav(index, nav) {

        for (var i = 0; i < index; i++) {
            nav.append('<div class="carousel-nav_dot"><span class="dot"></span></div>');
        }

    }

    // Переключает вкладки (страница товара, сертификаты, контакты)

    $(document).on('click touch', '.js-nav-tabs .tab', function () {

        var index = $(this).index();
        var ths = $(this);
        var tabs = $(this).parent().find('.active');
        var containerItems = $(this).parent().next().find('.tab-container_item');

        if ($(this).parent().next().hasClass('alphabet-basic')) {
            containerItems = $(this).parent().next().next().find('.tab-container_item');
        }

        if ($(this).parent().hasClass('flickity-slider')) {
            containerItems = $(this).parent().parent().parent().next().find('.tab-container_item');
        }

        // переопределяем переменную, если это на странице Контакты
        if ($(this).parent().parent().hasClass('magazine-catalog_heading')) {
            containerItems = $(this).parent().parent().next().find('.tab-container_item');
        }

        if (ths.hasClass('active')) {
            return false;
        }

        tabs.removeClass('active');
        containerItems.removeClass('open');
        ths.addClass('active');

        containerItems.each(function () {
            if ($(this).index() == index) {
                $(this).addClass('open');
            }
        });


    });

    // переключение вкладок (страница: оформление заказа)

    $(document).on('click touch', '.js-nav-tabs_checkout .tab', function () {

        var currentContainer;
        var index = $(this).index();
        var ths = $(this);
        var tabs = $(this).parent().find('.active');
        var containerItems = $(this).parent().next().find('.tab-container_item');

        // если класс у таба активен, то заканчиваем выполнение функции

        if (ths.hasClass('active') || ths.hasClass('active js-jurist-allow')) {
            return false;
        }

        tabs.removeClass('active'); //удаляем класс у кнопок табов
        containerItems.removeClass('open'); // скрываем неактивные блоки
        containerItems.find('input').removeAttr('disabled'); //активируем inputs
        containerItems.find('input').removeClass('error'); //очищаем от класса error
        containerItems.find('span.error').remove(); //удаляем лишние элементы
        ths.addClass('active'); //добавляем активному табу класс

        containerItems.each(function () {
            // сравниваем с индексом таба и показываем если индекс тот же
            if ($(this).index() == index) {
                $(this).addClass('open');
            }
            // добавляем input'ам disabled, чтобы выключить их из обработки
            else {
                $(this).find('input').prop('disabled', true);
            }
        });
    });

    $(document).on('click touch', '.js-open-affiliate', function () {
            // открываем/закрываем блок с филлиалами
            var parent = $(this).parent();

            parent.toggleClass('active');

            setTimeout(function () {
                parent.find('.affiliate-block_drop-list').addClass('js-is-open');
            }, 200);
        })
        .on('click touch', function (e) { // событие клика по веб-документу
            // скрываем блок, если клик не по нему
            var elem = $(".js-is-open");

            if (!elem.is(e.target) && elem.has(e.target).length === 0) {
                elem.closest('.active').removeClass('active').end().removeClass('js-is-open'); // скрываем его
                elem.prev().removeClass('active').end().removeClass('js-is-open'); // скрываем его
            }
        })
        .on('click touch', '.js-do-parent-active', function () {
            // Добавляет класс active к родительскому элементу 
            $(this).parent().addClass('active');
        })
        .on('click touch', '.js-toggle-sub', function () {
            // открывает/закрывает подкатегорию на мобильных
            $(this).parent().toggleClass('active');
        })
        .on('click touch', '.js-do-active', function () {
            // Добавляет класс active к текущему элементу
            $(this).toggleClass('active');
        })
        .on('click touch', '.js-do-active-popup', function () {
            // Добавляет класс active к текущему элементу
            var thsNext = $(this).next();

            $(this).toggleClass('active');

            setTimeout(function () {
                thsNext.addClass('js-is-open');
            }, 500);
        })
        .on('click touch', '.js-trigger-next', function () {
            // Добавляет класс active следующему элементу
            var thsNext = $(this).next();

            thsNext.toggleClass('active');

            setTimeout(function () {
                thsNext.addClass('js-is-open');
            }, 500);
        })
        .on('click touch', '.js-slide-next', function () {

            var thsNext = $(this).next();
            $(this).toggleClass('active');
            thsNext.slideToggle('350');
            thsNext.toggleClass('is-open');

        })
        .on('click touch', '.js-carousel-nav-category .carousel-item', function () {
            // открыть/закрыть подкатегории мобильного меню
            var ths = $(this);
            var thsID = $(this).attr('id');

            console.log(thsID);

            if (ths.hasClass('active')) {
                return false;
            }

            $('.nav-category_submenu .nav-category_drop-menu').css('display', 'none');
            $('.js-nav-category .carousel-item').removeClass('active');

            ths.toggleClass('active');

            $(".nav-category_submenu").find('#drop-' + thsID).css('display', 'block');

        })
        .on('click touch', '.js-nav-showall', function () {
            // открываем каталог категорий(в полную высоту) на мобильных
            $('.nav-category.changing-mobile .nav-category_menu').addClass('is-full-height');

            $('.nav-category.changing-mobile .nav-category_menu li').each(function () {
                $(this).show();
            });
            $(this).css('display', 'none');

        })
        .on('click touch', '.js-close-mobile', function () {
            $(this).closest('.mobile.active').removeClass('active js-is-open');
        })
        .on('click touch', '.js-filter-toggle', function () {
            $(this).toggleClass('active');
            $(this).next().slideToggle(400);
        })
        .on('click touch', '.js-click-change', function () {

            //  Добавляем активную/неактивную иконку рейтинга
            var index = $(this).index();

            if ($(this).hasClass('is-active') && (index == 5)) {
                return false;
            }

            $('.js-click-change').each(function () {
                if ($(this).index() > index) {
                    $(this).removeClass('is-active');
                } else if (!$(this).hasClass('is-active')) {
                    $(this).addClass('is-active');
                }
            });

        })
        .on('click touch', '.js-filter-close', function () {
            $('.filter form').removeClass('active js-is-open');
        })
        .on('mouseenter mouseleave', '.js-desktop-nav-category > li', function () {
            $(this).closest('.nav-category').toggleClass('z-index-is-magnified');
        })
        .on('click touch', 'a.js-parent-toggle', function (e) {
        
            e.preventDefault();
        
            $(this).parent().parent().find('.is-active').removeClass('is-active');
            $(this).parent().toggleClass('is-active');
        });


    $.masked = function () {
        var $el = $('input[type=tel]');
        $el.mask("+7 (000) 000-00-00", {
            placeholder: "Телефон"
        });
    }

    // Копируем описание в Карточке товара

    $(window).resize(function () {
        if ($(window).width() <= '1233') {
            $('.js-product-desc > *').each(function () {
                $(this).appendTo('.js-product-desc_mobile');
            });
        } else {
            $('.js-product-desc_mobile > *').each(function () {
                $(this).appendTo('.js-product-desc');
            });
        }
    });

    // Копируем доп.ссылки и вставляем в слайдер с превью в Карточке товара

    var linkInner = [];

    $('.js-carousel-modeling > a').each(function (index) {
        linkInner[index] = $(this).html();
    });

    $('.js-nav-product .carousel-nav_link a').each(function (index) {
        $(this).html(linkInner[index]);
    });

    // Рейтинг

    $.ratingSelect = function () {
        $('.js-rating').prepend($('<input type="text" class="vote-id" required name="detail_text" data-msg="Поставьте оценку">'));
        $(document).on('mouseenter', '.js-rating .rating-star', function () {
            $(this).prevAll('.rating-star').addClass('is-active');
            $(this).addClass('is-active');
            //            var $newImg = 'images/icon/star2.png';
            //            $('.hover').find('svg').attr('data-png', $newImg);
        });
        $(document).on('mouseleave', '.js-rating .rating-star', function () {
            $(this).nextAll('.rating-star').removeClass('is-active');
            $('.js-rating .rating-star').removeClass('is-active');
            //            var $oldImg = 'images/icon/star.png';
            //            $('.rating-star:not(.is-active):not(.ok)').find('svg').attr('data-png', $oldImg)
        });
        $(document).on('click', '.js-rating .rating-star', function () {
            $(this).prevAll('.rating-star').addClass('is-selected');
            $(this).addClass('is-selected');
            $(this).nextAll('.rating-star').removeClass('is-selected');
            var $newImg = 'images/star2.png';
            $(this).find('svg').attr('data-png', $newImg);
            var count = $('.js-rating .rating-star.is-selected').size();
            $('.vote-id').val(count);
            $('#rating-error').empty();
        });
    }

    // Копируем мобильное меню

    var arrowRightSvg = '<svg style="width: 0.56rem; height: 0.81rem;" class="float-r"><use xlink:href="#icon-angle-right"></use></svg>',
        menuMobileUserFavorite = $('.menu-block.mobile .user-tools_favorite .pos-relative'),
        menuMobileUserCart = $('.menu-block.mobile .user-tools_cart .pos-relative'),
        menuNavMenuWrapClass = $('.menu-block .nav-category.carousel'),
        menuNavSubmenuWrapClass = $('.menu-block .nav-category_submenu'),
        headerUserToolFavorite = $('.user-tools.in-header .user-tools_favorite').html(),
        headerUserToolCart = $('.user-tools.in-header .user-tools_cart').html();


    /* Добавляем кнопку для пользователя в моб.меню */
    $(function () {
        $('.user-tools.in-header .user-tools_auth > .btn').clone().addClass('float-r').appendTo($('.menu-block.mobile .mobile-block_top'));
    });

    /* Добавляем филлиалы в моб.меню */

    $(function () {
        $('.affiliate_contacts-wrap .affiliate-block').clone().appendTo($('.menu-block.mobile .affiliate-block_wrap'));
        $('.affiliate-block_wrap .affiliate-block').find('svg').remove();
        $('.affiliate-block_wrap .affiliate-block .affiliate-block_icon').append(arrowRightSvg);
        $('.affiliate-block_wrap .affiliate-block').find('.js-open-affiliate').removeClass('border-b-dashed js-open-affiliate');
        $('.affiliate-block_wrap .affiliate-block').find('.affiliate-block_icon').addClass('js-open-affiliate');
    });

    /* Добавляем корзину и избранное в моб.меню */

    menuMobileUserFavorite.append(headerUserToolFavorite);
    menuMobileUserCart.append(headerUserToolCart);

    /* Добавляем контакты */

    $('.affiliate_contacts-wrap .contacts-block > a').clone().each(function () {
        $(this).appendTo($('.menu-block.mobile .contacts-block'));
    });

    /* Добавляем навигацию в моб.меню */

    $('header nav a').clone().each(function () {
        $(this).appendTo($('.menu-block.mobile nav'));
    });

    /* Добавляем адресса и контакты из footer в моб.меню */

    $('footer .js-clone-in-menu').clone().each(function () {
        $(this).appendTo($('.menu-block.mobile .mobile-footer'));
    });

    /* Счетчик в карточке товара */

    $(document).on('click touch', '.js-change-add', function () {
        var inp = $(this).parent().find('input:text');
        var curNumbInp = parseInt(inp.val());
        curNumbInp++;
        inp.val(curNumbInp + '');
        inp.keyup();
        return false;
    });

    $(document).on('click touch', '.js-change-remove', function () {
        var inp = $(this).parent().find('input:text');
        var curNumbInp = parseInt(inp.val());
        if (curNumbInp > 1) {
            curNumbInp--;
            inp.val(curNumbInp + '');
            inp.keyup();
        }
        return false;
    });

    //    $(document).on('click', '.js-change-amount .change-del', function () {
    //        var inp = $(this).siblings('input:text');
    //        inp.val('1');
    //
    //        return false;
    //    });

    /* Считаем габариты в карточке товара */

    $.countDimensions = function () {

        // если не открылся попап с габаритами, то пропускаем функцию         
        if (!$('.modal-body').hasClass('dimensions')) return;

        var prodWidth = $('.js-proportional-img').data('width'),
            prodHeight = $('.js-proportional-img').data('height'),
            sCurHeight = $('.js-silhouette').height();

        $('.js-proportional-img').css({
            'width': (prodWidth / 180) * sCurHeight,
            'height': (prodHeight / 180) * sCurHeight
        });

        $(window).resize(function () {

            sCurHeight = $('.js-silhouette').height();

            $('.js-proportional-img').css({
                'width': (prodWidth / 180) * sCurHeight,
                'height': (prodHeight / 180) * sCurHeight
            });
        });
    }

    /* "Собираем" слайдер в моб.меню  */

    var headerNavMenu = $('.nav-category .nav-category_menu').clone()
        .removeClass('js-desktop-nav-category').addClass('js-nav-category js-carousel-nav-category')
        .each(function () {
            $(this).find('> li:not([id])').remove();
            $(this).find('li').addClass('carousel-item');
            $(this).find('li > a').attr('href', '#');
            $(this).find('.nav-category_angle').remove();
            $(this).find('.nav-category_drop-menu:not(".get-more")').clone().addClass('alt-layout').appendTo(menuNavSubmenuWrapClass);
        });

    $('.nav-category_drop-menu.get-more .sub-category').clone().each(function () {
        $(this).find('.nav-category_angle').remove();
        $(this).removeClass('sub-category').addClass('carousel-item').appendTo(headerNavMenu);
        $(this).find('.nav-category_drop-menu').addClass('alt-layout').appendTo(menuNavSubmenuWrapClass);
    });

    menuNavMenuWrapClass.append(headerNavMenu);

    /* *** */

}(jQuery));
