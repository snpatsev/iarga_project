'use strict';
var del = require('del');
var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var inject = require('gulp-inject');
var prettify = require('gulp-prettify');
var bowerFiles = require('main-bower-files');
var fileinclude = require('gulp-file-include');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var order = require("gulp-order");
var reload = browserSync.reload;
var fs = require('fs');
var cleanCSS = require('gulp-clean-css');

var hasBower = false;

fs.readdir('./bower_components', function(err) {
  if (!err) hasBower = true;
});

function serve(cb) {
  browserSync.init({
    server: './build'
  });
  cb();
}

var injectOrder = [
  'fonts.css',
  'base.css',
  'tablet.css',
  'phablet.css',
  'phone.css',
  'jquery.js',
  'modernizr-custom.js',
  'nospace.js',
  'jquery.maskedinput.min.js',
  'jquery-ui.min.js',
  'svg-sprite.js',
  'nospace.js',
  'slick.js',
  'textarea-autosize.js',
  'base.js',
  'base-init.js'
];

var paths = {
  src: {
    html: 'src',
    scss: 'src/scss',
    js: 'src/js',
    img: 'src/images',
    fonts: 'src/fonts'
  },
  build: {
    html: 'build',
    css: 'build/css',
    js: 'build/js',
    img: 'build/images',
    fonts: 'build/fonts'
  }
};

var files = {
  watch: {
    scss: paths.src.scss + '/**/*.scss',
    js: paths.src.js + '/**/*.js',
    html: paths.src.html + '/**/*.html',
    img: paths.src.img + '/**/*.*',
    fonts: paths.src.fonts + '/**/*.*'
  },
  src: {
    scss: paths.src.scss + '/*.scss',
    js: paths.src.js + '/**/*.js',
    html: paths.src.html + '/**/*.html',
    img: paths.src.img + '/**/*.*',
    fonts: paths.src.fonts + '/**/*.*'
  },
  build: {
    css: paths.build.css + '/*.css',
    js: paths.build.js + '/**/*.js',
    html: paths.build.html + '/*.html',
    img: paths.build.img + '/**/*.*',
    fonts: paths.build.fonts + '/**/*.*'
  }
};

var watch = {
  js: function () {
    var watcher = gulp.watch([files.watch.js]);
    watcher.on('change', gulp.series(build.js, reload));
    watcher.on('add', gulp.series(clear.js, build.js, build.html, reload));
    watcher.on('unlink', gulp.series(clear.js, build.js, build.html, reload));
  },
  scss: function () {
    var watcher = gulp.watch([files.watch.scss]);
    watcher.on('change', build.wcss);
    watcher.on('add', gulp.series(clear.css, build.css, build.html, reload));
    watcher.on('unlink', gulp.series(clear.css, build.css, build.html, reload));
  },
  html: function () {
    var watcher = gulp.watch([files.watch.html]);
    watcher.on('change', gulp.series(build.html, reload));
    watcher.on('add', build.html, reload);
    watcher.on('unlink', build.html, reload);
  },
  img: function () {
    var watcher = gulp.watch([files.watch.img]);
    watcher.on('change', gulp.series(build.img, reload));
    watcher.on('add', gulp.series(clear.img, build.img, reload));
    watcher.on('unlink', gulp.series(clear.img, build.img, reload));
  },
  fonts: function () {
    var watcher = gulp.watch([files.watch.fonts]);
    watcher.on('change', gulp.series(build.fonts, reload));
    watcher.on('add', gulp.series(clear.fonts, build.fonts, reload));
    watcher.on('unlink', gulp.series(clear.fonts, build.fonts, reload));
  },
  bower: function () {
    var watcher = gulp.watch(['bower.json']);
    watcher.on('all', function () {
      fs.readdir('./bower_components', function(err, files) {
        if (err) return;
        hasBower = true;
        if (files.length) {
          clear.js(gulp.series(build.bower, build.js, build.html, reload));
        } else {
          clear.js(gulp.series(build.js, build.html, reload));
        }
      });
    });
  }
};

watch.js.displayName = 'watch:js';
watch.scss.displayName = 'watch:scss';
watch.html.displayName = 'watch:html';
watch.img.displayName = 'watch:img';
watch.fonts.displayName = 'watch:fonts';
watch.bower.displayName = 'watch:bower';

watch.all = gulp.parallel(watch.js, watch.scss, watch.img, watch.fonts, watch.html, watch.bower);

var clear = {
  js: function (cb) {
    del(files.build.js);
    cb();
  },
  css: function (cb) {
    del(files.build.css);
    cb();
  },
  html: function (cb) {
    del(files.build.html);
    cb();
  },
  img: function (cb) {
    del(files.build.img);
    cb();
  },
  fonts: function (cb) {
    del(files.build.fonts);
    cb();
  }
};

clear.js.displayName = 'clear:js';
clear.css.displayName = 'clear:css';
clear.html.displayName = 'clear:html';
clear.img.displayName = 'clear:img';
clear.fonts.displayName = 'clear:fonts';

clear.all = gulp.series(clear.js, clear.css, clear.img, clear.fonts, clear.html);

var build = {
  html: function () {
    return gulp.src(files.src.html)
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(inject(
        gulp.src([files.build.css, files.build.js, "!./build/css/map.css"], {read: false})
          .pipe(order(injectOrder)),
        {
          ignorePath: 'build',
          addRootSlash: false,
          removeTags: true
        }
    ))
    .pipe(prettify({ indent_size: 2 }))
    .pipe(gulp.dest(paths.build.html));
  },
  js: function () {
    return gulp.src(files.src.js)
      .pipe(gulp.dest(paths.build.js));
  },
  css: function () {
    return gulp.src(files.src.scss)
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
      .pipe(cleanCSS())
      .pipe(gulp.dest(paths.build.css));
  },
  wcss: function () {
    return gulp.src(files.src.scss)
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
      .pipe(gulp.dest(paths.build.css))
      .pipe(browserSync.stream());
  },
  bower: function (cb) {
    if (hasBower) {
      return gulp.src(bowerFiles(), { base:'./bower_components', debugging: true })
        .pipe(rename(function(path){ path.dirname = ''; }))
        .pipe(gulp.dest('./' + paths.build.js));
    } else {
      cb();
    }
  },
  img: function () {
    return gulp.src(files.src.img)
      .pipe(gulp.dest(paths.build.img));
  },
  fonts: function () {
    return gulp.src(files.src.fonts)
      .pipe(gulp.dest(paths.build.fonts));
  }
};

build.js.displayName = 'build:js';
build.css.displayName = 'build:css';
build.wcss.displayName = 'build:wcss';
build.bower.displayName = 'build:bower';
build.img.displayName = 'build:img';
build.html.displayName = 'build:html';
build.fonts.displayName = 'build:fonts';

build.libs = gulp.series(clear.all, gulp.series(build.js, build.css, build.bower, build.img, build.fonts));
build.all = gulp.series(build.libs, build.html);

exports.build = build.all;
exports.watch = watch.all;
exports.clear = clear.all;

exports.default = gulp.series(build.all, gulp.series(serve, watch.all));