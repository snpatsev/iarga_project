(function ($) {

    $(function () {

        // Если нет поддержки тега <svg>
        if (!Modernizr.inlinesvg) {

            $('[data-png]').each(function () {

                var src = $(this).data('png');

                $(this).before($('<img src="' + src + '" alt="">')).remove();

            });

        }


        // Если нет поддержки картинки image.svg
        if (!Modernizr.svgasimg) {

            $('img[src $= ".svg"]').each(function () {

                var src = $(this).attr('src');

                $(this).attr('src', src.replace('.svg', '.png'));

            });

        }

    });

    //Слайдер на главной
    $.carouselFill = function () {

        var $el = $('.js-carousel-fill');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'left',
            lazyLoad: 1,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: 5500,
            prevNextButtons: false,
            pageDots: false,
            wrapAround: true,
            arrowShape: 'm69.15407,99.74611l-43.70131,-49.82702l43.70131,-49.83328l5.66058,6.45484l-38.04074,43.37844l38.04074,43.36172l-5.66058,6.4653z'
        });

        var flickity = $el.data('flickity');

        var $galleryNav = $('.Carousel-nav');
        var $galleryNavItems = $galleryNav.find('.Carousel-dot');

        $el.on('select.flickity', function () {
            $galleryNav.find('.is-selected').removeClass('is-selected');
            $galleryNavItems.eq(flickity.selectedIndex).addClass('is-selected');
        });

        $galleryNav.on('click', '.Carousel-dot', function () {
            var index = $(this).index();
            $el.flickity('select', index);
        });

        function onLoadeddata(event) {
            var cell = $el.flickity('getParentCell', event.target);
            $el.flickity('cellSizeChange', cell && cell.element);
        }

        $el.on('lazyLoad.flickity', function (event, cellElement) {
            $el.flickity('resize');
        });

        cancelTouchTap($el);


        /* Gallery Nav */

        function responsiveNav() {
            var $navHeight = $galleryNav.height(),
                $navLength = $galleryNav.children().length,
                $navItemHeight = $navHeight / $navLength;

            var $navItem = $(".Carousel-nav .Carousel-dot");
            $navItem.css({
                height: $navItemHeight,
                lineHeight: $navItemHeight + 'px'
            });
        }

        responsiveNav();

        $(window).on("resize", function () {
            setTimeout(function () {
                responsiveNav()
            }, 300);
        });
    };

    function cancelTouchTap($el) {
        $el.on('dragStart.flickity', function (event, pointer) {
            document.ontouchmove = function (e) {
                e.preventDefault();
            }
        });

        $el.on('dragEnd.flickity', function (event, pointer) {
            document.ontouchmove = function (e) {
                return true;
            }
        });
    }

    //Слайдер новостей на главной
    $.carouselNews = function () {

        var $el = $('.js-carousel-news');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'center',
            lazyLoad: 1,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: 5500,
            prevNextButtons: false,
            pageDots: true,
            wrapAround: true
        });

        cancelTouchTap($el);
    };

    //Слайдер Карточка товара
    $.carouselProduct = function () {

        var $el = $('.js-carousel-product');

        if (!$el.length) return;

        $el.flickity({
            cellAlign: 'center',
            lazyLoad: 2,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            autoPlay: false,
            prevNextButtons: false,
            pageDots: true,
            wrapAround: true,
            adaptiveHeight: true
        });

        var flickity = $el.data('flickity');

        // кастомная навигация с превью

        var wrapToggle = false;

        if ($('.Carousel-nav .Carousel-dot').length > 4) {
            wrapToggle = true;
        }

        $('.Carousel-nav').flickity({
            asNavFor: '.js-carousel-product',
            lazyLoad: 3,
            cellAlign: 'center',
            imagesLoaded: true,
            wrapAround: wrapToggle,
            groupCells: true,
            contain: true,
            dragThreshold: Modernizr.touchevents ? 10 : 3,
            pageDots: false,
            arrowShape: {
                x0: 10,
                x1: 60,
                y1: 50,
                x2: 65,
                y2: 40,
                x3: 25
            }
        });

        cancelTouchTap($el);

    };

    //    $(window).resize(function(){
    //        
    //       $(this).width() <= 1199 ? $.carouselProduct : false;
    //    });

    /* Попап галерея в Карточке товара */

    $.popupGallery = function (el) {

        var $el = el;

        if (!$el.length) return;

        $el.magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Загрузка #%curr%...',
            mainClass: 'mfp-img-mobile',
            closeMarkup: '<span class="mfp-close close"><svg style="width: 0.75rem; height: 0.75rem;"><use xlink:href="#icon-close"></use></svg></span>',
            tClose: 'Закрыть',
            gallery: {
                enabled: true,
                arrowMarkup: '<button type="button" class="mfp-arrow mfp-arrow-%dir%"><svg style="width: 3.19rem; height: 3.63rem;" class="flickity-button-icon %dir% mfp-prevent-close" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,90 L 25,50  L 65,10 L 60,0 Z"></path></svg></button>',
                tCounter: '<span class="mfp-counter-item">%curr% из %total%</span>',
                navigateByImgClick: true
            },
            image: {
                tError: '<a href="%url%">Изображение не найдено',
            },
            callbacks: {
                open: function () {
                    $('.mfp-arrow').appendTo($('.mfp-content'));
                }
            }
        });
    };

    /* Добавляем класс is-hover */

    $('.js-carousel-product').hover(function () {
        $(this).toggleClass('is-hover');
    });

    /* *** */

    var domain = location.hostname,
        imgMenu = "<img src='" + domain + "/images/icons/icon-burger.png' alt='Мобильное меню'></div>";
    
//    console.log(imgMenu);
    
    // Мобильное меню
    var $headerMobile = $('<div class="mob-menu">'),
        $headerMobileMenuDrop = $("<div class='burger js-mmenutrigger'>" + imgMenu + "</div>"),
        $headerMobileMenu = $('nav.menu').clone(),
        $headerMobileMenuBg = $('<div class="mob-menu_right-side"><svg class="cursor-p" style="width: 0.81rem; height: 0.81rem;"><use xlink:href="#icon-close"></use></svg></div>'),
        $headerMobileCity = $('header .user-geoposition').clone(),
        $headerMobilePhone = $('header .get-feedback_block').clone();

    $headerMobile
        .append($headerMobileMenuDrop)
        .append($headerMobileMenu)
        .append($headerMobileMenuBg)
        .appendTo($('header .content-wrap'));

    $headerMobile.find($headerMobileMenu)
        .append($headerMobilePhone)
        .append($headerMobileCity);

    $headerMobileMenuDrop.on('click touchstart', function (e) {
        if ($headerMobileMenu.is(':animated')) return false;
        $(this).toggleClass('active');
        $headerMobileMenu.toggleClass('visible');
        return false
    });
    $headerMobileMenuBg.on('click touchstart', function (e) {
        $headerMobileMenu.toggleClass('visible');
        return false
    });
    $(document).on('touchstart', function (e) {
        if (!$(e.target).closest(".menu.visible").length) {
            $('.header-mobile .link-drop').removeClass('active');
            $(".header-mobile .menu").slideUp(300).removeClass('visible');
        }
        e.stopPropagation();
    });

    // Попап
    $.popupAjax = function (el) {

        var $el = el;

        if (!$el.length) return;

        $el.magnificPopup({
            closeMarkup: '<span class="mfp-close close"><svg style="width: 0.75rem; height: 0.75rem;"><use xlink:href="#icon-close"></use></svg></span>',
            type: 'ajax',
            overflowY: 'scroll',
            preloader: false,
            removalDelay: 300,
            tClose: 'Закрыть',
            tLoading: 'Загрузка...',
            callbacks: {
                ajaxContentAdded: function () {
                    $.masked();
                    $('form').each(function () {
                        $.Valid($(this));
                    });
                }
            }
        });

    };

    //validator
    $.Valid = function (el) {

        var $el = el;
        if (!$el.length) return;

        var validator = $el.validate({

            errorElement: 'span',

            rules: {
                email: {
                    required: true,
                    email: true
                },
                name: "required",
                phone: "required",
                password: "required",
                message: "required",
                city: "required",
                address: "required"
            },

            messages: {
                email: {
                    required: 'Заполните поле!',
                    email: 'Неверно заполнено поле!'
                },
                name: {
                    required: 'Заполните поле!'
                },
                phone: {
                    required: 'Заполните поле!'
                },
                message: {
                    required: 'Ваше сообщение!'
                },
                city: {
                    required: 'Ваш город!'
                },
                address: {
                    required: 'Ваш адрес!'
                }
            }
        })

    }
    
    

    /* *** */

    $(document).on('click touch', '.js-searchtoggle', function (e) {

        $('#search').toggleClass('active');

        setTimeout(function () {
            $('#search input[name=search]').focus();
            $('.search-block_drop-input').addClass('popup');
        }, 500);
    });

    $(document).on('click touch', '.js-toggle-this', function () {
        $(this).toggleClass('active');
    });

    $(document).on('click touchstart', '.js-toggle', function () {
        $(this).parent().toggleClass('active');
    });

    /* Geo block */

    $(document).on('click touch', '.js-open-geo', function () {
        $('.user-geoposition').toggleClass('active');

        setTimeout(function () {
            $('.user-geoposition_drop-list').addClass('popup');
        }, 50);
    });

    $(document).on('click touch', '.js-close-geo', function () {
        $('.user-geoposition').removeClass('active');

        setTimeout(function () {
            $('.user-geoposition_drop-list').removeClass('popup');
        }, 50);
    });

    /* Блок с доп.ссылками в таблице */

    $(document).on('click touch', '.js-extra-block', function () {
        var ths = $(this);

        ths.parent().addClass('active');

        setTimeout(function () {
            ths.parent().find('.extra-block').addClass('popup');
        }, 50);

    });

    $(document).on('click touch', '.js-close-extra', function () {
        $(this).parent().parent().removeClass('active');
        $(this).parent().parent().find('.extra-block').removeClass('popup');
    });

    /* Remove/Undo in Cart Page */

    $(document).on('click touch', '.js-remove', function () {
        $(this).parent().parent().toggleClass('removed');
    });

    /* *** */

    /* Scroll to block */

    $(document).on('click touch', '.js-scrollto', function () {
        var block = '.' + $(this).data('href');
        $(block).addClass('open');

        $('html, body').animate({
            scrollTop: $(block).offset().top
        }, 1500);
    });

    /* *** */

    $(document).on('click touch', '.js-show', function () {
        $(this).next().slideToggle().end().parent().toggleClass('open');
    });

    /* Tab Menu */

    $(".category-container .js-tab_container .js-tab_item").not(".first").hide();
    $(document).on('click touch', ".js-tabs .js-tab", function () {
        $(this).closest('.category-container').find('.active').removeClass("active");
        $(this).addClass("active");
        $(this).closest('.category-container').find(".js-tab_container .js-tab_item").hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass("active");

    /* *** */

    //Макса для номера телефона
    
    $.masked = function () {
        var $el = $('input[type=tel]');

        if (!$el.length) return;

        $el.mask("+7 (999) 999-99-99", {
            placeholder: "+7 (XXX) XXX-XX-XX"
        });
    }

    // скрываем блок, если клик не по нему

    $(document).on('click touch', function (e) { // событие клика по веб-документу
        var elem = $(".popup");
        var elemGeo = $(".popup .drop-list_wrap");

        setTimeout(function () {
            if (!elem.is(e.target) && elem.has(e.target).length === 0) {
                elem.parent().removeClass('active').end().removeClass('popup'); // скрываем его
            }

            if (!elemGeo.is(e.target) && elemGeo.has(e.target).length === 0) {
                elemGeo.parent().parent().removeClass('active').end().removeClass('popup'); // скрываем его
            }
        }, 100);

    });
    
    $(document).on('click touch', '.js-catalog-list-toggle', function(){
        if ($(window).width() < 768){
            $(this).parent().parent().toggleClass('is-open');
        }
    });

}(jQuery));
