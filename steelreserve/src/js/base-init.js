$(function () {


    $(function () {
        // Если нет поддержки тега <svg>
        if (!Modernizr.inlinesvg) {

            $('[data-png]').each(function () {

                var src = $(this).data('png');

                $(this).before($('<img src="' + src + '" alt="">')).remove();

            });

        }


        // Если нет поддержки картинки image.svg
        
        if (!Modernizr.svgasimg) {

            $('img[src $= ".svg"]').each(function () {

                var src = $(this).attr('src');

                $(this).attr('src', src.replace('.svg', '.png'));

            });

        }
    });

    $('form').each(function(){
        $.Valid($(this));
    });
    
    // Карусели на главной

    $.carouselFill();

    $.carouselNews();

    /* *** */

    /* Карусель в Карточке товара */

    $.carouselProduct();

    $.popupGallery(
    $('.gallery-popup')
    );
    
    /* *** */

    $.popupAjax(
        $('.js-popup')
    );
    
    $.masked();
    
    $.noSpace();

}(jQuery));
